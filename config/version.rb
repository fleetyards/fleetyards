# frozen_string_literal: true

module Fleetyards
  CODENAME = 'Mercury'
  VERSION = 'v4.6.4'
end
