import emailTaken from './emailTaken'
import fidTaken from './fidTaken'
import usernameTaken from './usernameTaken'
import user from './user'
import url from './url'
import hexColor from './hexColor'

export { emailTaken, fidTaken, usernameTaken, user, url, hexColor }
