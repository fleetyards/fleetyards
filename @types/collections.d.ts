type Pagination = {
  currentPage: number
  totalPages: number
}

type CollectionParams = {
  page?: number
  cacheId?: string
}
